/*
 * (C) 2016 PROPAGANDIST CORPORATION
 * 
 * This program and the accompanying materials are made available
 * under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 */
package jp.co.propagandist.harmony;

public abstract class DesktopDetector {

    private final Desktop desktop;

    public DesktopDetector(Desktop desktop) {
        this.desktop = desktop;
    }

    public abstract boolean match(String text);

    public Desktop get() {
        return this.desktop;
    }

}