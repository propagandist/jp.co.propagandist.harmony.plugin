/*
 * (C) 2016 PROPAGANDIST CORPORATION
 * 
 * This program and the accompanying materials are made available
 * under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 */
package jp.co.propagandist.harmony;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;

public class Desktops {

    private static final Desktops instance = new Desktops();

    private final OS os;

    private final Desktop desktop;

    private Desktops() {
        switch (Platform.getOS()) {
        case Platform.OS_WIN32:
            os = OS.Windows;
            desktop = Desktop.Windows;
            break;
        case Platform.OS_MACOSX:
            os = OS.MacOSX;
            desktop = Desktop.MacOSX;
            break;
        case Platform.OS_LINUX:
            os = OS.Linux;
            desktop = getLinuxDesktop();
            break;
        default:
            os = OS.Unknown;
            desktop = Desktop.Unknown;
            break;
        }
    }

    public OS getOS() {
        return this.os;
    }

    public Desktop getDesktop() {
        if (desktop == null) {

        }
        return this.desktop;
    }

    private Desktop getLinuxDesktop() {
        final String[] cmdLine = new String[] { "ps", "ax" }; //$NON-NLS-1$ //$NON-NLS-2$
        final DesktopDetector[] detectors = new DesktopDetector[] { new CinnamonDetector(), new MateDetector(),
                new XfceDetector(), new UnityDetector(), new PantheonDetector(), new KdeDetector(), new GnomeDetector(),
                new LxdeDetector() };
        try {
            final Process process = Runtime.getRuntime().exec(cmdLine);
            process.waitFor();
            try (BufferedReader r = new BufferedReader(new InputStreamReader(process.getInputStream()))) {
                for (String line = r.readLine(); line != null; line = r.readLine()) {
                    for (DesktopDetector detector : detectors) {
                        if (detector.match(line)) {
                            return detector.get();
                        }
                    }
                }
            }
        } catch (Throwable th) {
            Activator.log(IStatus.ERROR, Messages.Desktops_CommandError + String.join(" ", cmdLine), th); // $NON-NLS-2$
        }
        return Desktop.Unknown;
    }

    public static Desktops getInstance() {
        return instance;
    }

    // Fedra 24 Workstation (2016/09/19)
    // debian 8 jessie with GNOME (2016/09/20)
    private static class GnomeDetector extends DesktopDetector {

        public GnomeDetector() {
            super(Desktop.LinuxGNOME);
        }

        @Override
        public boolean match(String text) {
            return text.contains("gnome-settings-daemon"); //$NON-NLS-1$
        }

    }

    // Linux Mint 18 Sarah with Cinnamon (2016/09/19)
    private static class CinnamonDetector extends DesktopDetector {

        public CinnamonDetector() {
            super(Desktop.LinuxCINNAMON);
        }

        @Override
        public boolean match(String text) {
            return text.contains("cinnamon-settings-daemon"); //$NON-NLS-1$
        }

    }

    // Linux Mint 18 Sarah with MATE (2016/09/19)
    private static class MateDetector extends DesktopDetector {

        public MateDetector() {
            super(Desktop.LinuxMATE);
        }

        @Override
        public boolean match(String text) {
            return text.contains("mate-settings-daemon"); //$NON-NLS-1$
        }

    }

    // Zorin OS 9.1 core (2016/09/19)
    // Ubuntu 16.04 (2016/09/19)
    private static class UnityDetector extends DesktopDetector {

        public UnityDetector() {
            super(Desktop.LinuxUNITY);
        }

        @Override
        public boolean match(String text) {
            return text.contains("unity-settings-daemon"); //$NON-NLS-1$
        }

    }

    // elementary OS 0.4 Loki (2016/09/19)
    private static class PantheonDetector extends DesktopDetector {

        public PantheonDetector() {
            super(Desktop.LinuxPANTHEON);
        }

        @Override
        public boolean match(String text) {
            return text.contains("pantheon"); //$NON-NLS-1$
        }

    }

    // Linux Mint 18 Sarah with KDE (2016/09/19)
    // SolydK 201606 (2016/09/20)
    // debian 8 jessie with KDE (2016/09/20)
    private static class KdeDetector extends DesktopDetector {

        public KdeDetector() {
            super(Desktop.LinuxKDE);
        }

        @Override
        public boolean match(String text) {
            return text.contains("kdeinit"); //$NON-NLS-1$
        }

    }

    // Linux Mint 18 Sarah with Xfce (2016/09/19)
    // SolydX 201606 (2016/09/20)
    // debian 8 jessie with Xfce (2016/09/20)
    private static class XfceDetector extends DesktopDetector {

        public XfceDetector() {
            super(Desktop.LinuxXFCE);
        }

        @Override
        public boolean match(String text) {
            return text.contains("xfwm4"); //$NON-NLS-1$
        }

    }

    // Lubuntu 16.04 (2016/09/20)
    // debian 8 jessie with LXDE (2016/09/20)
    private static class LxdeDetector extends DesktopDetector {

        public LxdeDetector() {
            super(Desktop.LinuxLXDE);
        }

        @Override
        public boolean match(String text) {
            return text.contains("LXDE"); //$NON-NLS-1$
        }

    }

}
