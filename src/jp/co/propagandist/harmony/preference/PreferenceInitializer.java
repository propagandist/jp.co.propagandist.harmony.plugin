/*
 * (C) 2016 PROPAGANDIST CORPORATION
 * 
 * This program and the accompanying materials are made available
 * under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 */
package jp.co.propagandist.harmony.preference;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.jface.preference.IPreferenceStore;

import jp.co.propagandist.harmony.Activator;
import jp.co.propagandist.harmony.Command;
import jp.co.propagandist.harmony.Commands;
import jp.co.propagandist.harmony.Preference;

public class PreferenceInitializer extends AbstractPreferenceInitializer {

    public PreferenceInitializer() {
    }

    @Override
    public void initializeDefaultPreferences() {
        final IPreferenceStore store = Activator.getDefault().getPreferenceStore();
        final Command command = Commands.get();
        if (command != null) {
            store.setDefault(Preference.OPEN_FOLDER_COMMAND, command.getFiler());
            store.setDefault(Preference.OPEN_TERMINAL_COMMAND, command.getTerminal());
        } else {
            store.setDefault(Preference.OPEN_FOLDER_COMMAND, ""); //$NON-NLS-1$
            store.setDefault(Preference.OPEN_TERMINAL_COMMAND, ""); //$NON-NLS-1$
        }
    }

}
