/*
 * (C) 2016 PROPAGANDIST CORPORATION
 * 
 * This program and the accompanying materials are made available
 * under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 */
package jp.co.propagandist.harmony;

import static jp.co.propagandist.harmony.Command.PATH_VAR;

import java.util.HashMap;
import java.util.Map;

public final class Commands {

    private static final Map<Desktop, Command> COMMAND_MAP = new HashMap<>();

    static {
        COMMAND_MAP.put(Desktop.Windows, Command.of("cmd.exe /c start", "explorer.exe " + PATH_VAR)); //$NON-NLS-1$ //$NON-NLS-2$
        COMMAND_MAP.put(Desktop.MacOSX, Command.of("/usr/bin/open -a /Applications/Utilities/Terminal.app " + PATH_VAR, //$NON-NLS-1$
                "/usr/bin/open -a /System/Library/CoreServices/Finder.app " + PATH_VAR)); //$NON-NLS-1$
        COMMAND_MAP.put(Desktop.LinuxGNOME,
                Command.of("gnome-terminal --working-directory=" + PATH_VAR, "nautilus " + PATH_VAR)); //$NON-NLS-1$ //$NON-NLS-2$
        COMMAND_MAP.put(Desktop.LinuxCINNAMON,
                Command.of("gnome-terminal --working-directory=" + PATH_VAR, "nemo " + PATH_VAR)); //$NON-NLS-1$ //$NON-NLS-2$
        COMMAND_MAP.put(Desktop.LinuxKDE, Command.of("konsole --workdir " + PATH_VAR, "dolphin " + PATH_VAR)); //$NON-NLS-1$ //$NON-NLS-2$
        COMMAND_MAP.put(Desktop.LinuxMATE,
                Command.of("mate-terminal --working-directory=" + PATH_VAR, "caja " + PATH_VAR)); //$NON-NLS-1$ //$NON-NLS-2$
        COMMAND_MAP.put(Desktop.LinuxXFCE,
                Command.of("xfce4-terminal --working-directory=" + PATH_VAR, "thunar " + PATH_VAR)); //$NON-NLS-1$ //$NON-NLS-2$
        COMMAND_MAP.put(Desktop.LinuxUNITY,
                Command.of("gnome-terminal --working-directory=" + PATH_VAR, "nautilus " + PATH_VAR)); //$NON-NLS-1$ //$NON-NLS-2$
        COMMAND_MAP.put(Desktop.LinuxPANTHEON,
                Command.of("pantheon-terminal --working-directory=" + PATH_VAR, "pantheon-files " + PATH_VAR)); //$NON-NLS-1$ //$NON-NLS-2$
        COMMAND_MAP.put(Desktop.LinuxLXDE,
                Command.of("lxterminal --working-directory=" + PATH_VAR, "pcmanfm " + PATH_VAR)); //$NON-NLS-1$ //$NON-NLS-2$
    }

    public static Command get() {
        final Desktop desktop = Desktops.getInstance().getDesktop();
        return COMMAND_MAP.get(desktop);
    }

}
