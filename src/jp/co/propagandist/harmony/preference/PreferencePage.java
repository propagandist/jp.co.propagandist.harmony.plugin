/*
 * (C) 2016 PROPAGANDIST CORPORATION
 * 
 * This program and the accompanying materials are made available
 * under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 */
package jp.co.propagandist.harmony.preference;

import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

import jp.co.propagandist.harmony.Activator;
import jp.co.propagandist.harmony.Preference;

public class PreferencePage extends FieldEditorPreferencePage implements IWorkbenchPreferencePage {

    public PreferencePage() {
        super(GRID);
        setPreferenceStore(Activator.getDefault().getPreferenceStore());
    }

    @Override
    public void init(IWorkbench workbench) {
    }

    @Override
    protected void createFieldEditors() {
        addField(new StringFieldEditor(Preference.OPEN_FOLDER_COMMAND, Messages.PreferencePage_LabelFiler,
                getFieldEditorParent()));
        addField(new StringFieldEditor(Preference.OPEN_TERMINAL_COMMAND, Messages.PreferencePage_LabelTerminal,
                getFieldEditorParent()));
    }

}
