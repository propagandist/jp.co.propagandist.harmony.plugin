/*
 * (C) 2016 PROPAGANDIST CORPORATION
 * 
 * This program and the accompanying materials are made available
 * under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 */
package jp.co.propagandist.harmony;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.ui.IStartup;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 */
public class Activator extends AbstractUIPlugin implements IStartup {

    /** The plug-in ID */
    public static final String PLUGIN_ID = "jp.co.propagandist.harmony.plugin"; //$NON-NLS-1$

    /** The plug-in Title */
    public static final String PLUGIN_TITLE = Messages.Activator_Title;

    /** The shared instance */
    private static Activator plugin;

    /**
     * The constructor
     */
    public Activator() {
    }

    /**
     * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
     */
    public void start(BundleContext context) throws Exception {
        super.start(context);
        plugin = this;

        final OS os = Desktops.getInstance().getOS();
        final Desktop desktop = Desktops.getInstance().getDesktop();
        log(IStatus.INFO, "[Eclipse Harmony] OS=" + os + ", Desktop=" + desktop); //$NON-NLS-1$ //$NON-NLS-2$
    }

    /**
     * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
     */
    public void stop(BundleContext context) throws Exception {
        plugin = null;
        super.stop(context);
    }

    /**
     * Returns the shared instance
     *
     * @return the shared instance
     */
    public static Activator getDefault() {
        return plugin;
    }

    public static IStatus log(int severity, final String message) {
        final IStatus status = new Status(severity, PLUGIN_ID, message);
        getDefault().getLog().log(status);
        return status;
    }

    public static IStatus log(int severity, final String message, final Throwable throwable) {
        final IStatus status = new Status(severity, PLUGIN_ID, message, throwable);
        getDefault().getLog().log(status);
        return status;
    }

    public static IStatus log(IStatus status) {
        getDefault().getLog().log(status);
        return status;
    }

    @Override
    public void earlyStartup() {
    }

}
