/*
 * (C) 2016 PROPAGANDIST CORPORATION
 * 
 * This program and the accompanying materials are made available
 * under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 */
package jp.co.propagandist.harmony;

public class Command {

    public static final String PATH_VAR = "{path}"; //$NON-NLS-1$

    private final String terminal;
    private final String filer;

    public Command(String terminal, String filer) {
        super();
        this.terminal = terminal;
        this.filer = filer;
    }

    public static Command of(String terminal, String filer) {
        return new Command(terminal, filer);
    }

    public String getTerminal() {
        return terminal;
    }

    public String getFiler() {
        return filer;
    }

}
