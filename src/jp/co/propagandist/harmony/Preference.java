/*
 * (C) 2016 PROPAGANDIST CORPORATION
 * 
 * This program and the accompanying materials are made available
 * under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 */
package jp.co.propagandist.harmony;

public interface Preference {

    String OPEN_TERMINAL_COMMAND = "harmony.OPEN_TERMINAL_COMMAND"; //$NON-NLS-1$
    String OPEN_FOLDER_COMMAND = "harmony.OPEN_FOLDER_COMMAND"; //$NON-NLS-1$

}
