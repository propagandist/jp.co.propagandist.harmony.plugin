/*
 * (C) 2016 PROPAGANDIST CORPORATION
 * 
 * This program and the accompanying materials are made available
 * under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 */
package jp.co.propagandist.harmony.handlers;

import java.io.File;
import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Collectors;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.handlers.HandlerUtil;

import jp.co.propagandist.harmony.Activator;
import jp.co.propagandist.harmony.Command;

public abstract class BaseOpenHandler extends AbstractHandler {

    @Override
    public Object execute(ExecutionEvent event) throws ExecutionException {
        getCurrentFolder(event).ifPresent(dir -> {
            try {
                final String[] commandLine = getCommandLine(dir);
                DebugPlugin.exec(commandLine, dir);
            } catch (CoreException ex) {
                handleError(event, ex.getStatus());
            } catch (Throwable th) {
                final IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
                        Messages.BaseOpenHandler_RuntimeError + dir.getAbsolutePath(), th);
                handleError(event, status);
            }
        });
        return null;
    }

    protected void handleError(ExecutionEvent event, IStatus status) {
        Activator.log(status);
        final Shell shell = getActiveShell(event);
        ErrorDialog.openError(shell, Activator.PLUGIN_TITLE, Messages.BaseOpenHandler_ErrorDialogMessage, status);
    }

    protected Shell getActiveShell(ExecutionEvent event) {
        try {
            return HandlerUtil.getActiveShellChecked(event);
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return null;
    }

    protected abstract String getCommandString();

    protected String[] getCommandLine(File dir) throws CoreException {
        final String command = getCommandString();
        if (command == null || command.trim().equals("")) { //$NON-NLS-1$
            final IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID, Messages.BaseOpenHandler_CommandUnset);
            throw new CoreException(status);
        }
        return Arrays.stream(command.split("[ ]+")).map(s -> s.replace(Command.PATH_VAR, dir.getAbsolutePath())) //$NON-NLS-1$
                .collect(Collectors.toList()).toArray(new String[] {});
    }

    protected Optional<File> getCurrentFolder(ExecutionEvent event) {
        final IStructuredSelection ss = getCurrentStructuredSelection(event);
        if (!ss.isEmpty()) {
            final Object obj = ss.getFirstElement();
            if (obj instanceof IAdaptable) {
                final IResource resource = (IResource) ((IAdaptable) obj).getAdapter(IResource.class);
                if (resource != null) {
                    final File file = resource.getLocation().toFile();
                    if (!file.isDirectory()) {
                        return Optional.of(file.getParentFile());
                    }
                    return Optional.of(file);
                }
            }
        }
        return Optional.empty();
    }

    public static IStructuredSelection getCurrentStructuredSelection(ExecutionEvent event) {
        ISelection selection = HandlerUtil.getCurrentSelection(event);
        if (selection instanceof IStructuredSelection) {
            return (IStructuredSelection) selection;
        }
        return StructuredSelection.EMPTY;
    }

}
