/*
 * (C) 2016 PROPAGANDIST CORPORATION
 * 
 * This program and the accompanying materials are made available
 * under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 */
package jp.co.propagandist.harmony.handlers;

import org.eclipse.jface.preference.IPreferenceStore;

import jp.co.propagandist.harmony.Activator;
import jp.co.propagandist.harmony.Preference;

public class OpenTerminalHandler extends BaseOpenHandler {

    @Override
    protected String getCommandString() {
        final IPreferenceStore store = Activator.getDefault().getPreferenceStore();
        return store.getString(Preference.OPEN_TERMINAL_COMMAND);
    }

}
